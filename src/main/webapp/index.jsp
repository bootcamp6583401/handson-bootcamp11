<!doctype html>
<html>
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <script src="https://cdn.tailwindcss.com"></script>
</head>
<body class="h-screen flex justify-center items-center flex-col">
  <div class="p-4 rounded-md shadow-md bg-blue-600 text-white">
    <h1 class="text-3xl font-bold">
        Handson 1: Hello world!
      </h1>
  </div>

  <div class="my-4">
    <a href="/webapp/tester">Handson 2</a>
  </div>

  <h1 class="my-4">Handson 3</h1>
  <form action="/webapp/postbro" method="post" >
   <div class="flex gap-2 items-center">
    <label for="nama" class="font-semibold">Nama:</label>
    <input type="text" id="nama" name="nama" required
    placeholder="Nama kaw"
    class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
    >
   </div>
    <button type="submit" class="bg-blue-700 text-white px-2 py-1 rounded-md">Submit</button>
</form>

<div class="my-4">
    <h2>Handson 4</h2>
    <form action="/webapp/handson-4" method="post" class="space-y-2">
        <div>
            <label for="nama" class="block mb-2 text-sm font-medium text-gray-900 dark:text-white">Nama</label>
            <input type="text" id="nama" name="nama" class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500" placeholder="John" required>
        </div>
        <div>
            <label for="jenis_kelamin" class="block mb-2 text-sm font-medium text-gray-900 dark:text-white">Jen. Kel.</label>
            <input type="text" id="jenis_kelamin" name="jenis_kelamin" class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500" placeholder="John" required>
        </div>
        <div>
            <label for="email" class="block mb-2 text-sm font-medium text-gray-900 dark:text-white">Email</label>
            <input type="text" id="email" name="email" class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500" placeholder="John" required>
        </div>
        <div>
            <label for="warna_favorite" class="block mb-2 text-sm font-medium text-gray-900 dark:text-white">Warna Favorite</label>
            <input type="text" id="warna_favorite" name="warna_favorite" class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500" placeholder="John" required>
        </div>
         <button type="submit" class="bg-blue-700 text-white px-2 py-1 rounded-md">Submit</button>
     </form>
</div>
</body>
</html>