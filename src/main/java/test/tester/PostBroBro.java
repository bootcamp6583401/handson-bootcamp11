package test.tester;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

@WebServlet("/handson-4")
public class PostBroBro extends HttpServlet {

    private static final long serialVersionUID = 23454345L;

    public void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {

        res.setContentType("text/plain");

        BufferedReader reader = req.getReader();
        StringBuilder requestBody = new StringBuilder();
        String line;
        while ((line = reader.readLine()) != null) {
            requestBody.append(line);
        }

        String responseBody = requestBody.toString();

        String[] data = responseBody.split("&");

        PrintWriter writer = res.getWriter();
        writer.write("\nNama Anda: " + getData(data, 0));
        writer.write("\nJenis Kelamin Anda: " + getData(data, 1));
        writer.write("\nEmail Anda: " + getData(data, 2));
        writer.write("\nWarna Favorite Anda: " + getData(data, 3));

        writer.close();

    }

    public String getData(String[] data, int index) {
        return data[index].split("=")[1];
    }

}